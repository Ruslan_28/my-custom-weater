import React, { Component } from 'react';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    }
  }
  
  onChange = (e) => {
    this.setState({
      value: e.target.value
    })
    
    this.props.onSearch(e.target.value)
  }
  
  render() {
    // console.log(value)
    return (
      <input 
        className="searchCity"
        type="text"
        placeholder="Search City"
        value={this.state.value}
        onChange={this.onChange}
      />
    )
  }
}

export default Search;

