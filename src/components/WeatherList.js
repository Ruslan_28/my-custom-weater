import React, { Component } from 'react';
import WeatherCity from './WeatherCity';

class WeatherList extends Component {
  
  render() {
    return (
      <div className="weatherList_box">
        {
          !this.props.todos
          ?
          <div>кавоо</div>
          :
          this.props.todos.map(item => {
            return <WeatherCity item={item} key={item.id} getCitiInfo={this.props.getCitiInfo}/>
          })
        }
      </div>
    )
  }
}

export default WeatherList;