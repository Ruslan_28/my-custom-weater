import React, { Component } from 'react';

class CitiesList extends Component {
  
  onClick = (id) => {
    this.props.handelClick(id)
  }
  
  render() {
    return (
      <div className="CityList">
        {
          !this.props.todos 
          ?
          <div>стояять</div>
          :
          this.props.todos.map(item => {
            return (
              <div className="cityItem" key={item.id}>
                <div className="">
                  {item.city}
                </div>
                <div className="">
                  {item.country}
                </div>
                <i className="fa fa-plus" onClick={()=> this.onClick(item.id)}></i>
              </div>
            )
          })
        }
      </div>
    )
  }
}

export default CitiesList;