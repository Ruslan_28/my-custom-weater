import React, { Component } from 'react';

class ViewsWeater extends Component {
  
  onClick = (id) => {
    this.props.handleDelete(id)
  }
  
  render() {
    return (
      <div className="viewsWeater_box">
        {
          !this.props.todos 
          ?
          <div>какаоo</div>
          :
          this.props.todos.map(item => {
            return (
              <div className="viewsWeater" key={item.id}>
                <i className="fa fa-trash" onClick={()=> this.onClick(item.id)}></i>
                <div className="">
                  City: <span>{item.name}</span>
                </div>
                <div className="">
                  Country: <span>{item.sys.country}</span>
                </div>
                <div className="">
                  Temperature: <span>{item.main.temp}C</span>
                </div>
                <div className="">
                  Sky: <span>{item.weather[0].description}</span>
                </div>
                <div className="">
                  Window: <span>{item.wind.speed} m/s</span>
                </div>
                <div className="">
                  Visibility: <span>{item.visibility} m</span>
                </div>
                <div className="">
                  Humidity: <span>{item.main.humidity}</span>
                </div>
              </div>
            )
          })
        }
      </div>
    )
  }
}

export default ViewsWeater;