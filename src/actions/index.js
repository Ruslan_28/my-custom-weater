import axios from 'axios';

export const SEARCH_CITIES = "SEARCH_CITIES";
export const ADD_WETHER_CITY = "ADD_WETHER_CITY";
export const GET_CITIES_ADD = "GET_CITIES_ADD";
export const DELETE_WETHER_CITY = "DELETE_WETHER_CITY";

export function searchCities(find) {
  return {
    type: SEARCH_CITIES,
    find
  }
}

export function addWeatherCity(id) {
  return {
    type: ADD_WETHER_CITY,
    id
  }
}

export function getCitiesAdd(getItem) {
  
  const { city, country } = getItem;
  
  return dispatch => {
    return axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=a827f02ff70c375299c478e090228d48&units=metric`)
      .then(res => res.data)
      .then(cities => dispatch({
        type: GET_CITIES_ADD,
        cities
      }))
  }
}

export function deleteWeatherCity(id) {
  return {
    type: DELETE_WETHER_CITY,
    id
  }
}