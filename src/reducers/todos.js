import { SEARCH_CITIES, ADD_WETHER_CITY, GET_CITIES_ADD, DELETE_WETHER_CITY } from '../actions';

import store from '../store/state';

const initialState = {
    data: store,
    weatherAdd: JSON.parse(localStorage.getItem('weatherAdd')) || [],
    weaterViews: JSON.parse(localStorage.getItem('weaterViews')) || []
}

export default function todos(state = initialState, action) {
  
  switch(action.type) {
    
    case SEARCH_CITIES:
          let filterTodos;
          
          if(action.find) {
              filterTodos = state.data.filter(item => item.city.toLowerCase().includes(action.find.toLowerCase()));
          }
          
          return {
              ...state,
              filterTodos,
          }
        
    case ADD_WETHER_CITY: {
      const weatherAdd = state.weatherAdd.concat();
      
      const { id } = action;
      
      const pushWeather = state.data.find(item => item.id === id);
      
      weatherAdd.push(pushWeather);
      
      localStorage.getItem('weatherAdd', JSON.stringify(weatherAdd));
      
      return {
        ...state,
        weatherAdd
      }
    }
    
    case GET_CITIES_ADD:
      const weaterViews = state.weaterViews.concat();
      
      weaterViews.push(action.cities)
      
      localStorage.setItem('weaterViews', JSON.stringify(weaterViews));
      
      return {
        ...state,
        weaterViews
      }
    
    case DELETE_WETHER_CITY: {
      const { id } = action;
      const weaterViews = state.weaterViews.filter( item => id !== item.id );
      const weatherAdd = state.weaterViews.filter( item => id !== item.id )
      
      localStorage.setItem('weaterViews', JSON.stringify(weaterViews));
      
      localStorage.setItem('weatherAdd', JSON.stringify(weatherAdd));
      
      return {
        ...state,
        weaterViews,
        weatherAdd
      }
    }
          
    default:
      return state
  }
}