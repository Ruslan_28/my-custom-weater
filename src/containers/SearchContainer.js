import { connect } from 'react-redux';

import Search from '../components/Search';
import { searchCities } from '../actions';

function mapDispatchToProps(dispatch) {
  return {
    onSearch: find => dispatch(searchCities(find))
  }
}

const SearchContainer = connect(null, mapDispatchToProps)(Search);

export default SearchContainer;