import { connect } from 'react-redux';

import ViewsWeater from '../components/ViewsWeater';
import { deleteWeatherCity } from '../actions';

function mapStateToProps(store) {
  return {
    todos: store.todos.weaterViews
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handleDelete: id => dispatch(deleteWeatherCity(id))
  }
}

const ViewsWeaterContainer = connect(mapStateToProps, mapDispatchToProps)(ViewsWeater);

export default ViewsWeaterContainer;