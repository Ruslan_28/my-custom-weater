import { connect } from 'react-redux';
import { getCitiesAdd } from '../actions';

import WeatherList from '../components/WeatherList';

function mapStateToProps(store) {
  return {
    todos: store.todos.weatherAdd
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getCitiInfo: getItem => dispatch(getCitiesAdd(getItem))
  }
}

const WeatherListContainer = connect(mapStateToProps, mapDispatchToProps)(WeatherList);

export default WeatherListContainer;