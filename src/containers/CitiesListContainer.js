import { connect } from 'react-redux';

import CitiesList from '../components/CitiesList';
import { addWeatherCity } from '../actions';

function mapStateToProps(store) {
  return {
    todos: store.todos.filterTodos
  }
}

function mapDispatchToProps(dispatch) {
  return {
    handelClick: id => dispatch(addWeatherCity(id))
  }
}

const CitiesListContainer = connect(mapStateToProps, mapDispatchToProps)(CitiesList);

export default CitiesListContainer;