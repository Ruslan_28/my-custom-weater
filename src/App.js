import React, { Component } from 'react';
import './App.css';
import 'font-awesome/css/font-awesome.min.css';

import CitiesListContainer from './containers/CitiesListContainer';
import SearchContainer from './containers/SearchContainer';
import WeatherListContainer from './containers/WeatherListContainer';
import ViewsWeaterContainer from './containers/ViewsWeaterContainer';

class App extends Component {
  render() {
    return (
      <main>
        <SearchContainer />
        <CitiesListContainer />
        <WeatherListContainer />
        <ViewsWeaterContainer />
      </main>
    );
  }
}

export default App;
